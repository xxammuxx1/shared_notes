import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generic Cache Preload Service for loading entire tables into Redis
 * @param <T> Entity type
 * @param <ID> Primary key type
 */
@Service
public class CachePreloadService<T extends Serializable, ID extends Serializable> {

    private final JpaRepository<T, ID> repository;
    private final RedisTemplate<String, T> redisTemplate;

    @Autowired
    public CachePreloadService(
        JpaRepository<T, ID> repository, 
        RedisTemplate<String, T> redisTemplate
    ) {
        this.repository = repository;
        this.redisTemplate = redisTemplate;
    }

    /**
     * Preload entire table into Redis cache
     * @param cacheName Unique cache name/prefix
     * @return Number of items cached
     */
    public int preloadEntireCache(String cacheName) {
        // Fetch all entities from database
        List<T> entities = repository.findAll();

        // Cache each entity with a unique key
        List<String> cachedKeys = entities.stream()
            .map(entity -> {
                String key = generateCacheKey(cacheName, entity);
                redisTemplate.opsForValue().set(key, entity);
                return key;
            })
            .collect(Collectors.toList());

        // Optional: Set an expiration for cached items
        cachedKeys.forEach(key -> 
            redisTemplate.expire(key, Duration.ofHours(24))
        );

        return entities.size();
    }

    /**
     * Generate unique cache key for an entity
     * @param cacheName Base cache name
     * @param entity Entity to generate key for
     * @return Unique cache key
     */
    private String generateCacheKey(String cacheName, T entity) {
        // Use entity's class name and ID for unique key
        return cacheName + ":" + 
               entity.getClass().getSimpleName() + ":" + 
               extractEntityId(entity);
    }

    /**
     * Extract ID from entity (assumes entity has getId() method)
     * @param entity Entity to extract ID from
     * @return Entity ID as string
     */
    private String extractEntityId(T entity) {
        try {
            Method getIdMethod = entity.getClass().getMethod("getId");
            Object id = getIdMethod.invoke(entity);
            return id.toString();
        } catch (Exception e) {
            throw new RuntimeException("Could not extract ID from entity", e);
        }
    }
}

/**
 * Controller to trigger cache preload via REST endpoint
 */
@RestController
@RequestMapping("/api/cache")
public class CacheManagementController {

    private final Map<String, CachePreloadService<?, ?>> cachePreloadServices;

    @Autowired
    public CacheManagementController(
        List<CachePreloadService<?, ?>> services
    ) {
        this.cachePreloadServices = services.stream()
            .collect(Collectors.toMap(
                service -> service.getClass().getSimpleName(), 
                Function.identity()
            ));
    }

    /**
     * Trigger cache preload for a specific entity type
     * @param entityType Simple name of the entity's cache preload service
     * @return Response with number of items cached
     */
    @GetMapping("/preload")
    public ResponseEntity<String> preloadCache(
        @RequestParam String entityType
    ) {
        CachePreloadService<?, ?> service = cachePreloadServices.get(entityType + "CachePreloadService");
        
        if (service == null) {
            return ResponseEntity
                .badRequest()
                .body("No cache preload service found for: " + entityType);
        }

        try {
            int cachedItems = service.preloadEntireCache(entityType.toLowerCase());
            return ResponseEntity.ok(
                "Successfully cached " + cachedItems + " " + entityType + " items"
            );
        } catch (Exception e) {
            return ResponseEntity
                .internalServerError()
                .body("Failed to preload cache: " + e.getMessage());
        }
    }
}

/**
 * Example Entity-Specific Cache Preload Service
 */
@Service
public class UserCachePreloadService extends CachePreloadService<User, Long> {
    @Autowired
    public UserCachePreloadService(
        UserRepository userRepository, 
        RedisTemplate<String, User> redisTemplate
    ) {
        super(userRepository, redisTemplate);
    }
}

/**
 * Redis Configuration Enhancement
 */
@Configuration
public class RedisTemplateConfiguration {
    @Bean
    public RedisTemplate<String, Object> redisTemplate(
        RedisConnectionFactory connectionFactory
    ) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        
        // Configure serializers
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
        
        template.afterPropertiesSet();
        return template;
    }
}

/**
 * Configuration for Caching
 */
@Configuration
@EnableCaching
public class CacheConfiguration {
    @Bean
    public CacheManager cacheManager(RedisTemplate<String, Object> redisTemplate) {
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig()
            .entryTtl(Duration.ofHours(24))
            .serializeValuesWith(
                SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer())
            );

        return RedisCacheManager.builder(redisTemplate.getConnectionFactory())
            .cacheDefaults(config)
            .build();
    }
}
