import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.codec.StringCodec;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;

import com.azure.core.credential.AccessToken;
import com.azure.core.credential.TokenCredential;
import com.azure.core.credential.TokenRequestContext;
import com.azure.identity.DefaultAzureCredentialBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicReference;

@Configuration
public class RedisLettuceConfiguration {

    @Value("${azure.redis.host}")
    private String redisHost;

    @Value("${azure.redis.port}")
    private int redisPort;

    // Azure AD Token Management
    private final AtomicReference<String> currentAccessToken = new AtomicReference<>();
    private Instant tokenExpiration;

    private final TokenCredential tokenCredential;

    public RedisLettuceConfiguration() {
        // Initialize Azure AD Credential
        this.tokenCredential = new DefaultAzureCredentialBuilder()
            .build();
    }

    /**
     * Generate a new Azure AD access token
     * @return Current valid access token
     */
    private synchronized String refreshAccessToken() {
        try {
            // Request token with Redis-specific scope
            AccessToken accessToken = tokenCredential.getToken(
                new TokenRequestContext()
                    .addScopes("https://redis.azure.com/.default")
            ).block();

            if (accessToken != null) {
                String token = accessToken.getToken();
                currentAccessToken.set(token);
                tokenExpiration = accessToken.getExpiresAt();
                return token;
            }

            throw new RuntimeException("Failed to obtain Azure AD token");
        } catch (Exception e) {
            throw new RuntimeException("Token refresh failed", e);
        }
    }

    /**
     * Scheduled token refresh every 50 minutes 
     * (slightly before 55-minute expiration to ensure fresh token)
     */
    @Scheduled(fixedRate = 50 * 60 * 1000)
    public void scheduleTokenRefresh() {
        refreshAccessToken();
    }

    /**
     * Create Lettuce Redis Client with Azure AD authentication
     * @return StatefulRedisConnection for Redis operations
     */
    @Bean
    public StatefulRedisConnection<String, String> redisConnection() {
        // Ensure initial token is generated
        String initialToken = refreshAccessToken();

        // Configure Lettuce Client Resources
        ClientResources clientResources = DefaultClientResources.create();

        // Construct Redis URI with SSL and authentication
        RedisURI redisURI = RedisURI.builder()
            .withHost(redisHost)
            .withPort(redisPort)
            .withSsl(true)
            .withAuthentication(
                "access-token", 
                () -> currentAccessToken.updateAndGet(token -> 
                    (token == null || isTokenExpired()) ? refreshAccessToken() : token)
            )
            .build();

        // Create Redis Client
        RedisClient redisClient = RedisClient.create(clientResources, redisURI);

        // Open connection with String codec for key-value operations
        return redisClient.connect(StringCodec.UTF8);
    }

    /**
     * Check if current token is expired or about to expire
     * @return boolean indicating token expiration status
     */
    private boolean isTokenExpired() {
        return tokenExpiration == null || 
               Instant.now().isAfter(tokenExpiration.minus(Duration.ofMinutes(5)));
    }

    /**
     * Redis Operations Service
     */
    @Service
    public class RedisOperationsService {
        private final StatefulRedisConnection<String, String> redisConnection;

        @Autowired
        public RedisOperationsService(StatefulRedisConnection<String, String> redisConnection) {
            this.redisConnection = redisConnection;
        }

        /**
         * Set a value in Redis
         * @param key Redis key
         * @param value Value to store
         */
        public void set(String key, String value) {
            try {
                redisConnection.sync().set(key, value);
            } catch (Exception e) {
                // Implement retry or fallback mechanism
                throw new RuntimeException("Redis set operation failed", e);
            }
        }

        /**
         * Get a value from Redis
         * @param key Redis key
         * @return Stored value or null
         */
        public String get(String key) {
            try {
                return redisConnection.sync().get(key);
            } catch (Exception e) {
                // Implement retry or fallback mechanism
                throw new RuntimeException("Redis get operation failed", e);
            }
        }
    }

    /**
     * Custom Redis Repository for Entity Caching
     */
    @Repository
    public class RedisEntityRepository<T> {
        private final StatefulRedisConnection<String, String> redisConnection;
        private final ObjectMapper objectMapper;

        @Autowired
        public RedisEntityRepository(
            StatefulRedisConnection<String, String> redisConnection,
            ObjectMapper objectMapper
        ) {
            this.redisConnection = redisConnection;
            this.objectMapper = objectMapper;
        }

        /**
         * Save entity to Redis with JSON serialization
         * @param key Redis key
         * @param entity Entity to save
         */
        public void saveEntity(String key, T entity) {
            try {
                String jsonValue = objectMapper.writeValueAsString(entity);
                redisConnection.sync().set(key, jsonValue);
            } catch (Exception e) {
                throw new RuntimeException("Failed to save entity to Redis", e);
            }
        }

        /**
         * Retrieve entity from Redis
         * @param key Redis key
         * @param entityClass Class of the entity to deserialize
         * @return Deserialized entity or null
         */
        public T getEntity(String key, Class<T> entityClass) {
            try {
                String jsonValue = redisConnection.sync().get(key);
                return jsonValue != null 
                    ? objectMapper.readValue(jsonValue, entityClass) 
                    : null;
            } catch (Exception e) {
                throw new RuntimeException("Failed to retrieve entity from Redis", e);
            }
        }
    }

    /**
     * Bulk Cache Preload Controller
     */
    @RestController
    @RequestMapping("/api/cache")
    public class CachePreloadController {
        private final RedisEntityRepository<User> userRedisRepository;
        private final UserRepository userRepository;

        @Autowired
        public CachePreloadController(
            RedisEntityRepository<User> userRedisRepository,
            UserRepository userRepository
        ) {
            this.userRedisRepository = userRedisRepository;
            this.userRepository = userRepository;
        }

        /**
         * Preload all users into Redis cache
         * @return Number of users cached
         */
        @GetMapping("/preload/users")
        public ResponseEntity<String> preloadUserCache() {
            List<User> users = userRepository.findAll();
            
            users.forEach(user -> 
                userRedisRepository.saveEntity(
                    "user:" + user.getId(), 
                    user
                )
            );

            return ResponseEntity.ok(
                "Cached " + users.size() + " users successfully"
            );
        }
    }
}

# application.yml Configuration
spring:
  data:
    redis:
      repositories:
        enabled: true

azure:
  redis:
    host: your-redis-cache-name.redis.cache.windows.net
    port: 6380  # SSL Redis port

# Dependency Configuration (pom.xml)
<dependencies>
    <!-- Lettuce Redis Client -->
    <dependency>
        <groupId>io.lettuce</groupId>
        <artifactId>lettuce-core</artifactId>
        <version>6.2.4.RELEASE</version>
    </dependency>

    <!-- Azure Identity -->
    <dependency>
        <groupId>com.azure</groupId>
        <artifactId>azure-identity</artifactId>
        <version>1.11.1</version>
    </dependency>

    <!-- Jackson for JSON serialization -->
    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
    </dependency>
</dependencies>

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private OAuth2ResourceServerProperties resourceServerProperties;

    @Bean
    public RedisConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        config.setHostName(resourceServerProperties.getRedisHost());
        config.setPort(resourceServerProperties.getRedisPort());
        config.setPassword(resourceServerProperties.getAccessToken());
        return new LettuceConnectionFactory(config);
    }
}


