To achieve 10,000 requests per minute (RPM) with the NGINX Ingress Controller, you need to configure various aspects of NGINX to handle the required traffic efficiently. These configurations involve adjusting NGINX settings for connection handling, worker processes, and other relevant parameters in the NGINX Ingress Controller.

### Key Configuration Aspects:

1. *Increase Worker Processes and Connections*:
   You need to adjust the number of NGINX worker processes and the maximum number of connections each worker can handle.

   - *Worker processes*: The number of worker processes should be at least equal to the number of CPU cores available on the node. If you are scaling the Ingress Controller, consider using multiple replicas of the Ingress Controller.
   - *Worker connections*: This controls the maximum number of simultaneous connections each worker can handle. It should be high enough to handle the incoming traffic.

2. *Enable Keep-Alive and Optimize Timeouts*:
   Configuring timeouts and keep-alive settings can help reduce the load caused by frequent new connections. Using keep-alive ensures that connections are reused, reducing the overhead of setting up new connections.

3. *Increase Buffer and Queue Sizes*:
   Configure buffer and queue sizes to handle a larger volume of requests effectively.

4. *Rate Limiting* (Optional):
   If you want to throttle the number of requests and avoid excessive resource consumption, rate limiting can be configured. But if the goal is to handle 10,000 requests, make sure to tune the limits appropriately for your system.

### Steps to Configure NGINX Ingress Controller for 10,000 Requests per Minute

#### Step 1: Update the NGINX Ingress Controller ConfigMap
You can configure the NGINX Ingress Controller by modifying the ConfigMap used by the Ingress Controller.

1. *Edit the ConfigMap for NGINX Ingress Controller*:
   First, find and edit the ConfigMap associated with the NGINX Ingress Controller:

   bash
   kubectl get configmap -n <namespace> | grep nginx-ingress
   kubectl edit configmap <nginx-ingress-configmap> -n <namespace>
   

2. *Adjust worker_processes and worker_connections*:
   Update the values for worker_processes and worker_connections to allow the system to handle more connections.

   Example:

   yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: nginx-ingress-controller
     namespace: <namespace>
   data:
     worker-processes: "4"  # Use 4 worker processes (adjust based on your CPU cores)
     worker-connections: "2048"  # Allow up to 2048 connections per worker process
     multi_accept: "on"  # Allow workers to accept multiple connections at once
   

   - worker-processes: Set this to the number of CPU cores (or higher, depending on your server’s resources).
   - worker-connections: This value should be large enough to handle the required traffic. For example, 2048 means each worker can handle 2048 concurrent connections.

#### Step 2: Adjust the Deployment of NGINX Ingress Controller

1. *Update Resource Requests and Limits*:
   Adjust the resources to ensure that the NGINX Ingress Controller has enough CPU and memory to handle the load. For example:

   yaml
   resources:
     requests:
       cpu: "500m"
       memory: "256Mi"
     limits:
       cpu: "2"
       memory: "512Mi"
   

   This ensures that the NGINX Ingress Controller has at least the requested resources and can scale to the specified limits.

2. *Scale the Number of Replicas*:
   Depending on the traffic, you may need to scale the number of replicas of the Ingress Controller to distribute the load.

   bash
   kubectl scale deployment nginx-ingress-controller --replicas=3 -n <namespace>
   

   Adjust the replica count based on the load.

#### Step 3: Modify NGINX Timeouts and Buffer Sizes

To ensure that connections are handled efficiently, configure timeouts and buffer sizes:

yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-ingress-controller
  namespace: <namespace>
data:
  proxy-read-timeout: "600"
  proxy-send-timeout: "600"
  client-max-body-size: "50m"  # Increase client body size if needed
  http2-max-field-size: "16k"
  http2-max-header-size: "32k"
  large-client-header-buffers: "4 16k"  # Buffer large headers
  client-body-buffer-size: "128k"


- *proxy-read-timeout* and *proxy-send-timeout*: These settings determine how long the proxy will wait for reading the response and sending the request before timing out.
- *client-max-body-size*: This setting controls the maximum size of the client request body.
- *client-body-buffer-size*: Buffer size for handling larger request bodies.

#### Step 4: Enable Connection Keep-Alive

Enabling HTTP keep-alive improves performance by allowing multiple requests over a single TCP connection. This reduces the overhead of setting up new connections.

yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-ingress-controller
  namespace: <namespace>
data:
  http2: "true"  # Enable HTTP/2 for multiplexing
  keep-alive-timeout: "75"  # Set the keep-alive timeout


#### Step 5: Apply and Monitor

After updating the ConfigMap and deployment, apply the changes:

bash
kubectl apply -f <nginx-ingress-configmap>.yaml
kubectl rollout restart deployment nginx-ingress-controller -n <namespace>


Monitor the Ingress Controller logs and metrics to verify that it's handling the expected number of requests:

bash
kubectl logs -f <nginx-ingress-pod> -n <namespace>


You can also check resource usage to ensure the NGINX Ingress Controller is not being throttled:

bash
kubectl top pod <nginx-ingress-pod> -n <namespace>


### Optional: Rate Limiting (To Control Traffic)

If you need to limit incoming requests to avoid overloading the system, you can configure rate limiting.

For example, to limit the number of requests to 10,000 per minute, you could use the nginx.ingress.kubernetes.io/limit-rps annotation on your Ingress:

yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  annotations:
    nginx.ingress.kubernetes.io/limit-rps: "166"  # 10,000 requests per minute (10,000 / 60)
spec:
  rules:
    - host: example.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: example-service
                port:
                  number: 80


This ensures that the traffic doesn't exceed the desired rate.

### Conclusion

To handle 10,000 requests per minute, you'll need to configure the NGINX Ingress Controller for high throughput by increasing the number of worker processes, connections, and adjusting resource limits. Additionally, fine-tuning timeouts, buffer sizes, and enabling keep-alive will help efficiently handle the traffic.
