import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class RedisDataLoader {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * Load large list data to Redis in batches
     * @param key Redis key to store the list
     * @param data List of data to load
     * @param batchSize Number of items to load in each batch
     */
    public void loadListData(String key, List<?> data, int batchSize) {
        // Clear existing data for the key
        redisTemplate.delete(key);

        // Split data into batches and load
        for (int i = 0; i < data.size(); i += batchSize) {
            List<?> batch = data.subList(i, Math.min(data.size(), i + batchSize));
            redisTemplate.opsForList().rightPushAll(key, batch);
        }
    }

    /**
     * Load large map data to Redis in batches with optional expiration
     * @param key Redis key to store the map
     * @param data Map of data to load
     * @param batchSize Number of entries to load in each batch
     * @param expirationMinutes Optional expiration time in minutes (null for no expiration)
     */
    public void loadMapData(String key, Map<String, ?> data, int batchSize, Integer expirationMinutes) {
        // Clear existing data for the key
        redisTemplate.delete(key);

        // Convert map to list of entries for batch processing
        List<Map.Entry<String, ?>> entries = data.entrySet().stream().collect(Collectors.toList());

        // Load data in batches
        for (int i = 0; i < entries.size(); i += batchSize) {
            List<Map.Entry<String, ?>> batch = entries.subList(i, Math.min(entries.size(), i + batchSize));
            
            batch.forEach(entry -> 
                redisTemplate.opsForHash().put(key, entry.getKey(), entry.getValue())
            );
        }

        // Set expiration if specified
        if (expirationMinutes != null) {
            redisTemplate.expire(key, expirationMinutes, TimeUnit.MINUTES);
        }
    }

    /**
     * Load large set data to Redis in batches
     * @param key Redis key to store the set
     * @param data Set of data to load
     * @param batchSize Number of items to load in each batch
     */
    public void loadSetData(String key, List<?> data, int batchSize) {
        // Clear existing data for the key
        redisTemplate.delete(key);

        // Split data into batches and load
        for (int i = 0; i < data.size(); i += batchSize) {
            List<?> batch = data.subList(i, Math.min(data.size(), i + batchSize));
            redisTemplate.opsForSet().add(key, batch.toArray());
        }
    }
}
