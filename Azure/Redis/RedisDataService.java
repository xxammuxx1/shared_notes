@Service
public class DataService {
    @Autowired
    private RedisDataLoader redisDataLoader;

    public void loadLargeDataset(List<MyDataObject> largeDataset) {
        // Load list with 1000 items per batch
        redisDataLoader.loadListData("my:large:dataset", largeDataset, 1000);

        // Load map with 500 entries per batch and 1-hour expiration
        Map<String, MyDataObject> dataMap = // ... prepare your map
        redisDataLoader.loadMapData("my:large:map", dataMap, 500, 60);
    }
}