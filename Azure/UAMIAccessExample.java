import com.azure.identity.DefaultAzureCredential;
import com.azure.identity.DefaultAzureCredentialBuilder;
import com.azure.security.keyvault.secrets.SecretClient;
import com.azure.security.keyvault.secrets.SecretClientBuilder;

public class UAMIAccessExample {

    public static void main(String[] args) {
        // Replace with your Key Vault URL
        String keyVaultUrl = "https://<your-key-vault-name>.vault.azure.net/";

        // Create a DefaultAzureCredential
        DefaultAzureCredential credential = new DefaultAzureCredentialBuilder()
                .build();

        // Create a SecretClient
        SecretClient client = new SecretClientBuilder()
                .vaultUrl(keyVaultUrl)
                .credential(credential)
                .buildClient();

        // Access a secret from Key Vault
        String secretName = "mySecret";
        String secretValue = client.getSecret(secretName).getValue();

        System.out.println("Secret value: " + secretValue);
    }
}