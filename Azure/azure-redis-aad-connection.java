# Maven Dependencies (pom.xml)
<dependencies>
    <!-- Spring Boot Starter Data Redis -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>

    <!-- Azure Identity -->
    <dependency>
        <groupId>com.azure</groupId>
        <artifactId>azure-identity</artifactId>
        <version>1.11.1</version>
    </dependency>

    <!-- Azure Spring Boot Starter -->
    <dependency>
        <groupId>com.azure.spring</groupId>
        <artifactId>spring-boot-starter-azure-spring-cloud-starter</artifactId>
        <version>4.15.0</version>
    </dependency>
</dependencies>

# Redis Configuration Class
@Configuration
public class RedisConfiguration {

    @Value("${azure.redis.host}")
    private String redisHost;

    @Value("${azure.redis.port}")
    private int redisPort;

    @Value("${spring.application.name}")
    private String applicationName;

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        // Azure AD Token Provider Configuration
        AzureTokenCredentialResolver tokenCredentialResolver = new AzureTokenCredentialResolver();
        
        RedisConfiguration redisConfig = new RedisConfiguration();
        redisConfig.setHostName(redisHost);
        redisConfig.setPort(redisPort);
        
        // Configure SSL and AAD authentication
        LettuceClientConfiguration clientConfig = LettuceClientConfiguration.builder()
            .useSsl()
            .commandTimeout(Duration.ofSeconds(2))
            .shutdownTimeout(Duration.ZERO)
            .redisCredentials(tokenCredentialResolver)
            .build();
        
        return new LettuceConnectionFactory(redisConfig, clientConfig);
    }

    /**
     * Custom Token Credential Resolver for Azure AD Authentication
     */
    public class AzureTokenCredentialResolver implements RedisCredentials {
        private final TokenCredential tokenCredential;
        private String currentToken;
        private Instant tokenExpiration;

        public AzureTokenCredentialResolver() {
            // Use DefaultAzureCredentialBuilder for automatic credential detection
            this.tokenCredential = new DefaultAzureCredentialBuilder()
                .build();
        }

        @Override
        public String getUsername() {
            return refreshAccessToken();
        }

        @Override
        public String getPassword() {
            return "";  // No password, using AAD token
        }

        /**
         * Refresh Azure AD Access Token
         * @return Current valid access token
         */
        private synchronized String refreshAccessToken() {
            // Check if token is null or expired
            if (currentToken == null || isTokenExpired()) {
                try {
                    // Request new token with specific resource scope
                    AccessToken accessToken = tokenCredential.getToken(
                        new TokenRequestContext()
                            .addScopes("https://redis.azure.com/.default")
                            .setTenantId(System.getenv("AZURE_TENANT_ID"))
                    ).block();

                    if (accessToken != null) {
                        currentToken = accessToken.getToken();
                        tokenExpiration = accessToken.getExpiresAt();
                    }
                } catch (Exception e) {
                    throw new RuntimeException("Failed to obtain Azure AD token", e);
                }
            }
            return currentToken;
        }

        /**
         * Check if current token is expired
         * @return boolean indicating token expiration status
         */
        private boolean isTokenExpired() {
            return tokenExpiration == null || 
                   Instant.now().isAfter(tokenExpiration.minus(5, ChronoUnit.MINUTES));
        }
    }
}

# application.yml Configuration
spring:
  data:
    redis:
      repositories:
        enabled: true

azure:
  redis:
    host: your-redis-cache-name.redis.cache.windows.net
    port: 6380  # SSL Redis port

# Kubernetes Deployment Considerations
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: spring-redis-app
spec:
  template:
    spec:
      containers:
      - name: spring-app
        env:
        - name: AZURE_TENANT_ID
          valueFrom:
            secretKeyRef:
              name: azure-credentials
              key: tenant-id
        - name: AZURE_CLIENT_ID
          valueFrom:
            secretKeyRef:
              name: azure-credentials
              key: client-id
        - name: AZURE_CLIENT_SECRET
          valueFrom:
            secretKeyRef:
              name: azure-credentials
              key: client-secret
