import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ForkJoinPoolQueryDemo {
    public static void main(String[] args) {
        // Create a ForkJoinPool with the default parallelism level
        ForkJoinPool forkJoinPool = ForkJoinPool.commonPool();

        // List to store our Future results
        List<Future<String>> futures = new ArrayList<>();

        try {
            // 1. Database Query Simulation
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("User Profile Query", 2);
                return "Retrieved user profile data";
            }));

            // 2. API Call Simulation
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Weather API Query", 3);
                return "Fetched current weather information";
            }));

            // 3. Complex Calculation
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Financial Risk Calculation", 4);
                return "Computed complex financial risk model";
            }));

            // 4. Image Processing
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Image Thumbnail Generation", 2);
                return "Created image thumbnails";
            }));

            // 5. Machine Learning Prediction
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("ML Model Prediction", 5);
                return "Generated machine learning prediction";
            }));

            // 6. Log Analysis
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("System Log Analysis", 3);
                return "Analyzed system logs for anomalies";
            }));

            // 7. Network Resource Fetch
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Network Resource Download", 4);
                return "Downloaded network resources";
            }));

            // 8. Data Transformation
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Data Transformation", 2);
                return "Transformed complex data structure";
            }));

            // 9. Encryption Task
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Data Encryption", 3);
                return "Encrypted sensitive data";
            }));

            // 10. Aggregation Query
            futures.add(forkJoinPool.submit(() -> {
                simulateWork("Data Aggregation", 4);
                return "Completed complex data aggregation";
            }));

            // Process and print results
            for (Future<String> future : futures) {
                try {
                    System.out.println(future.get());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            // Shutdown the pool
            forkJoinPool.shutdown();
            try {
                if (!forkJoinPool.awaitTermination(10, TimeUnit.SECONDS)) {
                    forkJoinPool.shutdownNow();
                }
            } catch (InterruptedException e) {
                forkJoinPool.shutdownNow();
            }
        }
    }

    // Simulate work with a delay to mimic real-world processing
    private static void simulateWork(String taskName, int seconds) {
        System.out.println("Starting: " + taskName);
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        System.out.println("Completed: " + taskName);
    }
}
