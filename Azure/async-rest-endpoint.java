@RestController
@RequestMapping("/api")
public class AsyncController {
    
    private final ConcurrentHashMap<String, CompletableFuture<String>> taskMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, TaskStatus> statusMap = new ConcurrentHashMap<>();

    @Data
    @AllArgsConstructor
    public static class TaskStatus {
        private String status;
        private String result;
    }

    @GetMapping("/longrunning")
    public ResponseEntity<Map<String, String>> startLongRunningTask() {
        String taskId = UUID.randomUUID().toString();
        
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            try {
                statusMap.put(taskId, new TaskStatus("IN_PROGRESS", null));
                // Your long running task here
                Thread.sleep(180000); // 3 minutes
                String result = "Task completed successfully";
                statusMap.put(taskId, new TaskStatus("COMPLETED", result));
                return result;
            } catch (Exception e) {
                statusMap.put(taskId, new TaskStatus("FAILED", e.getMessage()));
                throw new CompletionException(e);
            }
        });

        taskMap.put(taskId, future);
        
        return ResponseEntity.ok(Map.of("taskId", taskId));
    }

    @GetMapping("/status/{taskId}")
    public ResponseEntity<TaskStatus> getTaskStatus(@PathVariable String taskId) {
        TaskStatus status = statusMap.get(taskId);
        if (status == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(status);
    }
}
