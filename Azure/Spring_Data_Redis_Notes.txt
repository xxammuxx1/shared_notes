# 
spring.datasource.url=jdbc:postgresql://your_postgres_host:your_postgres_port/your_database_name
spring.datasource.username=your_postgres_username
spring.datasource.password=your_postgres_password
spring.redis.host=localhost
spring.redis.port=6379
spring.redis.password=your_redis_password   
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration

#

@Entity
public class MyData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    // ... other fields
}

public interface MyDataRepository extends JpaRepository<MyData, Long> {
}

@Service
public class DataSynchronizationService {
    @Autowired
    private MyDataRepository myDataRepository;
    @Autowired
    private RedisTemplate<String, MyData> redisTemplate;

    @Scheduled(fixedRate = 60000) // Adjust the rate as needed
    public void synchronizeData() {
        List<MyData> dataList = myDataRepository.findAll();
        dataList.forEach(data -> {
            redisTemplate.opsForValue().set(data.getId().toString(), data);
        });
    }
}

@Service
public class DataSynchronizationService {
    @Autowired
    private MyDataRepository myDataRepository;
    @Autowired
    private RedisTemplate<String, MyData> redisTemplate;

    @Scheduled(fixedRate = 60000) // Adjust the rate as needed
    public void synchronizeData() {
        // Fetch data from Redis first
        List<MyData> dataList = new ArrayList<>();
        for (Long id : getIdsFromRedis()) {
            MyData data = redisTemplate.opsForValue().get(id.toString());
            if (data != null) {
                dataList.add(data);
            }
        }

        // If data is not found in Redis, fetch from Postgres and update Redis
        if (dataList.isEmpty()) {
            dataList = myDataRepository.findAll();
            dataList.forEach(data -> {
                redisTemplate.opsForValue().set(data.getId().toString(), data);
            });
        }

        // Process the data list
        // ...
    }

    private List<Long> getIdsFromRedis() {
        // Implement logic to retrieve a list of IDs from Redis, e.g., using Redis keys or a specific data structure
        // The ids should be prefixed with Entity Class Name
        // ...
    }
}

#
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
    <groupId>org.postgresql</groupId>
    <artifactId>postgresql</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
<dependency>
    <groupId>com.azure.spring</groupId>
    <artifactId>spring-cloud-azure-starter-data-redis-lettuce</artifactId>
</dependency>

#
apiVersion: batch/v1
kind: Job
metadata:
  name: data-sync-job
spec:
  template:
    spec:
      containers:
      - name: data-sync
        image: your-image-name:your-tag
        command: ["/bin/sh"]
        args: ["-c", "your-script-or-application-command"]
      restartPolicy: Never
	  
#
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: data-sync-cronjob
spec:
  schedule: "0 0 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: data-sync-1
            image: your-image-name-1:your-tag-1
            command: ["/bin/sh"]
            args: ["-c", "your-script-or-application-command-1"]
          - name: data-sync-2
            image: your-image-name-2:your-tag-2
            command: ["/bin/sh"]
            args: ["-c", "your-script-or-application-command-2"]
          restartPolicy: OnFailure
      backoffLimit: 5

#
kubectl apply -f cronjob.yaml

#
apiVersion: batch/v1
kind: Job
metadata:
  name: data-sync-job-1
spec:
  template:
    spec:
      containers:
      - name: data-sync-1
        image: your-image-name-1:your-tag-1
        command: ["/bin/sh"]
        args: ["-c", "your-script-or-application-command-1"]
      restartPolicy: OnFailure
      backoffLimit: 5

---
apiVersion: batch/v1
kind: Job
metadata:
  name: data-sync-job-2
spec:
  template:
    spec:
      containers:
      - name: data-sync-2
        image: your-image-name-2:your-tag-2
        command: ["/bin/sh"]
        args: ["-c", "your-script-or-application-command-2"]
      restartPolicy: OnFailure
      backoffLimit: 5
	  
#
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: data-sync-cronjob
spec:
  schedule: "0 0 * * *"  # Adjust the schedule as needed
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: trigger-jobs
            image: your-image-name:your-tag
            command: ["/bin/sh"]
            args: ["-c", "kubectl create job data-sync-job-1; kubectl create job data-sync-job-2"]
          restartPolicy: OnFailure
      backoffLimit: 5
	  
#create dependencies between Kubernetes Jobs

apiVersion: batch/v1
kind: Job
metadata:
  name: job-1
spec:
  template:
    spec:
      containers:
      - name: job-1-container
        image: your-image-1
        command: ["/bin/sh"]
        args: ["-c", "your-script-1"]
      restartPolicy: Never

---
apiVersion: batch/v1
kind: Job
metadata:
  name: job-2
spec:
  template:
    spec:
      containers:
      - name: job-2-container
        image: your-image-2
        command: ["/bin/sh"]
        args: ["-c", "your-script-2"]
      restartPolicy: Never
  depends_on:
  - name: job-1

apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: trigger-cronjob
spec:
  schedule: "0 0 * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: trigger-job
            image: your-image
            command: ["/bin/sh"]
            args: ["-c", "kubectl create job job-1; kubectl create job job-2 --depends-on=job-1"]
          restartPolicy: OnFailure
		  
#
Lombok’s @Data annotation, which adds @ToString, @EqualsAndHashCode, @Getter/@Setter, and a @RequiredArgsContructor

#
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-validation</artifactId>
</dependency>

#
A Spring Boot application can have many CommandLineRunners; to control the order of their execution, we can further annotate them with the @Order annotation.

# CreateUsers CommandLineRunner

package com.redislabs.edu.redi2read.boot;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.redislabs.edu.redi2read.models.Role;
import com.redislabs.edu.redi2read.models.User;
import com.redislabs.edu.redi2read.repositories.RoleRepository;
import com.redislabs.edu.redi2read.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Order(2)
@Slf4j
public class CreateUsers implements CommandLineRunner {

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @Override
  public void run(String... args) throws Exception {
    if (userRepository.count() == 0) {
      // load the roles
      Role admin = roleRepository.findFirstByname("admin");
      Role customer = roleRepository.findFirstByname("customer");

      try {
        // create a Jackson object mapper
        ObjectMapper mapper = new ObjectMapper();
        // create a type definition to convert the array of JSON into a List of Users
        TypeReference<List<User>> typeReference = new TypeReference<List<User>>() {
        };
        // make the JSON data available as an input stream
        InputStream inputStream = getClass().getResourceAsStream("/data/users/users.json");
        // convert the JSON to objects
        List<User> users = mapper.readValue(inputStream, typeReference);

        users.stream().forEach((user) -> {
          user.setPassword(passwordEncoder.encode(user.getPassword()));
          user.addRole(customer);
          userRepository.save(user);
        });
        log.info(">>>> " + users.size() + " Users Saved!");
      } catch (IOException e) {
        log.info(">>>> Unable to import users: " + e.getMessage());
      }

      User adminUser = new User();
      adminUser.setName("Adminus Admistradore");
      adminUser.setEmail("admin@example.com");
      adminUser.setPassword(passwordEncoder.encode("Reindeer Flotilla"));//
      adminUser.addRole(admin);

      userRepository.save(adminUser);
      log.info(">>>> Loaded User Data and Created users...");
    }
  }
}

#
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(value = { "password", "passwordConfirm" }, allowSetters = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@Data
@RedisHash
public class User {
...
}

#
 @GetMapping
 public Iterable<User> all(@RequestParam(defaultValue = "") String email) {
   if (email.isEmpty()) {
     return userRepository.findAll();
   } else {
     Optional<User> user = Optional.ofNullable(userRepository.findFirstByEmail(email));
     return user.isPresent() ? List.of(user.get()) : Collections.emptyList();
   }
 }
 
#
package com.redislabs.edu.redi2read.boot;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.redislabs.edu.redi2read.models.Book;
import com.redislabs.edu.redi2read.models.Category;
import com.redislabs.edu.redi2read.repositories.BookRepository;
import com.redislabs.edu.redi2read.repositories.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Order(3)
@Slf4j
public class CreateBooks implements CommandLineRunner {
  @Autowired
  private BookRepository bookRepository;

  @Autowired
  private CategoryRepository categoryRepository;

  @Override
  public void run(String... args) throws Exception {
    if (bookRepository.count() == 0) {
    ObjectMapper mapper = new ObjectMapper();
    TypeReference<List<Book>> typeReference = new TypeReference<List<Book>>() {
    };

    List<File> files = //
        Files.list(Paths.get(getClass().getResource("/data/books").toURI())) //
            .filter(Files::isRegularFile) //
            .filter(path -> path.toString().endsWith(".json")) //
            .map(java.nio.file.Path::toFile) //
            .collect(Collectors.toList());

    Map<String, Category> categories = new HashMap<String, Category>();

    files.forEach(file -> {
          try {
            log.info(">>>> Processing Book File: " + file.getPath());
            String categoryName = file.getName().substring(0, file.getName().lastIndexOf("_"));
            log.info(">>>> Category: " + categoryName);

            Category category;
            if (!categories.containsKey(categoryName)) {
            category = Category.builder().name(categoryName).build();
            categoryRepository.save(category);
            categories.put(categoryName, category);
            } else {
            category = categories.get(categoryName);
            }

            InputStream inputStream = new FileInputStream(file);
            List<Book> books = mapper.readValue(inputStream, typeReference);
            books.stream().forEach((book) -> {
            book.addCategory(category);
            bookRepository.save(book);
            });
            log.info(">>>> " + books.size() + " Books Saved!");
          } catch (IOException e) {
            log.info("Unable to import books: " + e.getMessage());
          }
    });

    log.info(">>>> Loaded Book Data and Created books...");
    }
  }
}

#

  @GetMapping
  public ResponseEntity<Map<String, Object>> all( //
    @RequestParam(defaultValue = "0") Integer page, //
    @RequestParam(defaultValue = "10") Integer size //
  ) {
    Pageable paging = PageRequest.of(page, size);
    Page<Book> pagedResult = bookRepository.findAll(paging);
    List<Book> books = pagedResult.hasContent() ? pagedResult.getContent() : Collections.emptyList();

    Map<String, Object> response = new HashMap<>();
    response.put("books", books);
    response.put("page", pagedResult.getNumber());
    response.put("pages", pagedResult.getTotalPages());
    response.put("total", pagedResult.getTotalElements());

    return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
  }
  
 #
 spring.redis.host=<your-redis-hostname>
spring.redis.port=6379
spring.redis.password=<your-redis-password>
spring.redis.timeout=6000
spring.redis.lettuce.pool.max-active=8
spring.redis.lettuce.pool.max-idle=8
spring.redis.lettuce.pool.min-idle=1
spring.redis.lettuce.pool.max-wait=-1


#
@Service
public class CachePreloadService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void preloadCache() {
        // Example data to preload
        Map<String, String> data = new HashMap<>();
        data.put("key1", "value1");
        data.put("key2", "value2");

        // Preload data into Redis cache
        data.forEach((key, value) -> {
            stringRedisTemplate.opsForValue().set(key, value);
        });
    }
}


# how to preload data from postgres table
To preload data from a PostgreSQL table into the cache, you can use the pg_prewarm module

CREATE EXTENSION pg_prewarm;

SELECT pg_prewarm('your_table_name');


CREATE EXTENSION pg_buffercache;
SELECT c.relname, count(*) AS buffers
FROM pg_class c
LEFT JOIN pg_buffercache b ON b.relfilenode = c.relfilenode
GROUP BY c.relname;


#
@Service
public class CachePreloadService {
    @Autowired
    private MyRepository myRepository;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void preloadCache() {
        List<MyEntity> entities = myRepository.findAll();
        entities.forEach(entity -> {
            stringRedisTemplate.opsForValue().set(entity.getId().toString(), entity);
        });
    }
}

#
@Service
public class MyService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private MyEntityRepository myEntityRepository;

    public MyEntity getEntity(Long id) {
        // Try to get from Redis Cache
        String cachedEntity = stringRedisTemplate.opsForValue().get(id.toString());
        if (cachedEntity != null) {
            // Convert cached JSON string to MyEntity object
            MyEntity entity = new ObjectMapper().readValue(cachedEntity, MyEntity.class);
            return entity;
        }

        // If not found in Redis, fetch from PostgreSQL
        Optional<MyEntity> entityOpt = myEntityRepository.findById(id);
        if (entityOpt.isPresent()) {
            MyEntity entity = entityOpt.get();
            // Cache the entity in Redis for future requests
            stringRedisTemplate.opsForValue().set(id.toString(), new ObjectMapper().writeValueAsString(entity));
            return entity;
        }

        // If not found in PostgreSQL, return null or handle accordingly
        return null;
    }
}

#
@RestController
@RequestMapping("/api")
public class MyController {
    @Autowired
    private MyService myService;

    @GetMapping("/entity/{id}")
    public ResponseEntity<MyEntity> getEntity(@PathVariable Long id) {
        MyEntity entity = myService.getEntity(id);
        if (entity != null) {
            return new ResponseEntity<>(entity, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}


#

@Service
public class CachePreloadService {
    @Autowired
    private TableARepository tableARepository;

    @Autowired
    private TableBRepository tableBRepository;

    // Autowire other repositories as needed

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void preloadCache() {
        // Preload Table A data into Redis
        List<TableAEntity> tableAData = tableARepository.findAll();
        tableAData.forEach(entity -> {
            stringRedisTemplate.opsForValue().set("TableA:" + entity.getId(), new ObjectMapper().writeValueAsString(entity));
        });

        // Preload Table B data into Redis
        List<TableBEntity> tableBData = tableBRepository.findAll();
        tableBData.forEach(entity -> {
            stringRedisTemplate.opsForValue().set("TableB:" + entity.getId(), new ObjectMapper().writeValueAsString(entity));
        });

        // Repeat for other tables as needed
    }
}


# Adding OpenAPI to Spring Boot 3.3.5
<dependency>
    <groupId>org.springdoc</groupId>
    <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
    <version>2.1.0</version>
</dependency>

#
http://localhost:8080/v3/api-docs

#
http://localhost:8080/swagger-ui.html

#
@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping("/{id}")
    @Operation(summary = "Get a user by ID", description = "Retrieves a user with the specified ID")
    public User getUserById(@PathVariable Long id) {
        // ...
    }
}

#
springdoc.show-actuator: true

#Jedis
https://www.baeldung.com/spring-data-redis-tutorial

#

 