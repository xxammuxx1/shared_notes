// LinkVisitService.java
@Service
@EnableAsync
public class LinkVisitService {
    private final LinkVisitRepository linkVisitRepository;
    private final AsyncVisitProcessor asyncVisitProcessor;

    @Autowired
    public LinkVisitService(LinkVisitRepository linkVisitRepository, AsyncVisitProcessor asyncVisitProcessor) {
        this.linkVisitRepository = linkVisitRepository;
        this.asyncVisitProcessor = asyncVisitProcessor;
    }

    // This method now returns immediately
    public void recordVisit(String url) {
        asyncVisitProcessor.processVisit(url);
    }

    public Long getVisitCount(String url) {
        return linkVisitRepository.findByUrl(url)
            .map(LinkVisit::getVisitCount)
            .orElse(0L);
    }
}

// AsyncVisitProcessor.java
@Component
public class AsyncVisitProcessor {
    private final LinkVisitRepository linkVisitRepository;
    private static final int BATCH_SIZE = 100;
    private final Queue<String> visitQueue = new ConcurrentLinkedQueue<>();
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    @Autowired
    public AsyncVisitProcessor(LinkVisitRepository linkVisitRepository) {
        this.linkVisitRepository = linkVisitRepository;
        // Schedule batch processing every 5 seconds
        scheduler.scheduleAtFixedRate(this::processBatch, 0, 5, TimeUnit.SECONDS);
    }

    @Async
    public void processVisit(String url) {
        visitQueue.offer(url);
    }

    @Transactional
    protected void processBatch() {
        Map<String, Long> visitCounts = new HashMap<>();
        List<String> batch = new ArrayList<>();
        
        // Collect up to BATCH_SIZE visits from the queue
        for (int i = 0; i < BATCH_SIZE && !visitQueue.isEmpty(); i++) {
            String url = visitQueue.poll();
            if (url != null) {
                batch.add(url);
                visitCounts.merge(url, 1L, Long::sum);
            }
        }

        if (batch.isEmpty()) {
            return;
        }

        // Bulk update visit counts
        List<LinkVisit> existingVisits = linkVisitRepository.findByUrlIn(batch);
        Map<String, LinkVisit> existingVisitsMap = existingVisits.stream()
            .collect(Collectors.toMap(LinkVisit::getUrl, Function.identity()));

        List<LinkVisit> visitsToSave = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();

        for (Map.Entry<String, Long> entry : visitCounts.entrySet()) {
            String url = entry.getKey();
            Long additionalVisits = entry.getValue();

            LinkVisit visit = existingVisitsMap.getOrDefault(url, new LinkVisit(url));
            if (visit.getId() != null) {
                visit.setVisitCount(visit.getVisitCount() + additionalVisits);
            }
            visit.setLastVisited(now);
            visitsToSave.add(visit);
        }

        linkVisitRepository.saveAll(visitsToSave);
    }

    @PreDestroy
    public void shutdown() {
        scheduler.shutdown();
        try {
            // Process remaining items before shutting down
            processBatch();
        } catch (Exception e) {
            // Log error
        }
    }
}

// LinkVisitRepository.java
@Repository
public interface LinkVisitRepository extends JpaRepository<LinkVisit, Long> {
    Optional<LinkVisit> findByUrl(String url);
    List<LinkVisit> findByUrlIn(Collection<String> urls);
}

// AsyncConfig.java
@Configuration
@EnableAsync
public class AsyncConfig implements AsyncConfigurer {
    
    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(4);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("VisitTracker-");
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }
}