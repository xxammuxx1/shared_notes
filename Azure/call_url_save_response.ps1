param(
    [string]$url = "https://example.com",
    [string]$outputPath = ".\responses",
    [int]$durationHours = 3
)

# Create output directory if it doesn't exist
if (!(Test-Path -Path $outputPath)) {
    New-Item -ItemType Directory -Force -Path $outputPath
}

# Calculate end time
$endTime = (Get-Date).AddHours($durationHours)

# Continuous loop with time limit
while ((Get-Date) -lt $endTime) {
    # Generate unique filename with timestamp
    $timestamp = Get-Date -Format "yyyyMMdd_HHmmss"
    $filename = Join-Path $outputPath "response_$timestamp.txt"
    
    try {
        # Make web request and save response
        Invoke-WebRequest -Uri $url -Method Get | Select-Object -ExpandProperty Content | Out-File $filename
        
        Write-Host "Response saved to $filename"
    }
    catch {
        Write-Host "Error fetching URL: $_"
    }
    
    # Wait 10 seconds before next request
    Start-Sleep -Seconds 10
}

Write-Host "Script completed after $durationHours hours"