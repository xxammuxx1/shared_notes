SELECT /*+ parallel(4) */ cu.res_nuid AS nuid,
  ro.email         AS resource_email_address
FROM rpt_global_rights g,
  srm_resources ro,
  odf_ca_resource cu
WHERE ro.id             = cu.id
AND cu.res_nuid        IS NOT NULL
AND g.right_group_name IN ('Timesheets - Approve All', 'Resource - Approve Time')
AND g.res_rec_id        = ro.id
UNION ALL
SELECT /*+ parallel(4) */ cu.res_nuid,
  ro.email
FROM rpt_instance_rights g,
  srm_resources ro,
  odf_ca_resource cu
WHERE ro.id             = cu.id
AND cu.res_nuid        IS NOT NULL
AND g.right_group_name IN ('Resource - Approve Time')
AND g.res_rec_id        = ro.id
AND EXISTS
  (SELECT 1
  FROM srm_resources rx,
    odf_ca_resource cx
  WHERE rx.id IN
    (SELECT UNIQUE a.object_instance_id
    FROM cmn_sec_assgnd_obj_perm a,srm_resources srm
    WHERE srm.entity_code='9601'
    and a.object_instance_id=srm.id
    and a.principal_id = ro.user_id
    AND a.principal_type = 'USER'
    AND a.right_id       = 3632 --Resource - Approve Time
    )
  AND rx.id = cx.id
  )
