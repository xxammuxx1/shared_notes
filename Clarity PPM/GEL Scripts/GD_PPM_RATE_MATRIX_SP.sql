create or replace PROCEDURE GD_PPM_RATE_MATRIX_SP (
   P_STAGING_ID      NUMBER,
   P_MATRIXNAME      VARCHAR2,
   P_RESOURCEID      VARCHAR2,
   P_RATECATEGORY    VARCHAR2,
   P_STARTDATE       DATE,
   P_TODATE          DATE,
   P_RATE            NUMBER 
)
-- ===================================================================================================================
-- Procedure Name:	  GD_PPM_RATE_MATRIX_SP
-- Description:       This Process validates and Create/Update the Rate Matrix
-- Author:            XXXXXXXXXX
-- Create date:       XXXXXXXXXX
-- Last Updated Date: XXXXXXXXXXX
-- ===================================================================================================================
AS
    V_VALIDRESOURCE 				INTEGER :=1			;
    V_CURRENTRATE 					NUMBER  :=0			;
    V_CURRENTSTARTDATE 				DATE				;
    V_CURRENTENDDATE 				DATE				;
    V_CURRENTROWID 					INTEGER				;
    V_MATRIXID 						INTEGER				;
    V_EXISTINGRECORD 				INTEGER :=0			;
	V_RATEINSERT 					INTEGER :=0			;
	V_RATEUPDATE 					INTEGER :=0			;
	V_RESOURCEEXISTS 				INTEGER :=0			;
	V_RATECATEGORY					VARCHAR2(30)		;
	V_RESOURCEID					VARCHAR2(30)		;
	V_RESOURCECOUNT					NUMBER				;
	V_ROWEXISTS						NUMBER				;
CURSOR CURRCURRENTROW IS
    SELECT MV.MATRIXROWKEY, MV.FROMDATE, MV.TODATE,  NUMVAL1 RATE,VALUE2 RATE_CATEGORY,VALUE1 RESOURCEID 
    FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
    WHERE M.DESCRIPTION =P_MATRIXNAME
      AND MV.MATRIXKEY = M.MATRIXKEY
      AND MV.VALUE1 = P_RESOURCEID
      AND MV.VALUE2 =P_RATECATEGORY
      AND MV.VALUE3 IS NULL
      AND MV.VALUE4 IS NULL
      AND MV.VALUE5 IS NULL
      AND MV.VALUE6 IS NULL
      AND MV.VALUE7 IS NULL
      AND MV.VALUE8 IS NULL
      AND MV.VALUE9 IS NULL
      AND MV.VALUE10 IS NULL
    ORDER BY MV.TODATE DESC;
BEGIN
 --DBMS_OUTPUT.PUT_LINE('Starting GD_PPM_RATE_MATRIX_SP.....' ||P_RESOURCEID|| ',' || P_RATECATEGORY || ',' || P_TODATE || ',' || P_RATE || ',' || P_STARTDATE);
	BEGIN /* Deleting the Rate matrix from CA PPM where Start Date greater than termination date Start*/
	   SELECT COUNT(*) INTO V_RESOURCECOUNT
	   FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
	   WHERE M.DESCRIPTION =P_MATRIXNAME
	   	  AND MV.MATRIXKEY = M.MATRIXKEY
	   	  AND MV.VALUE1 = P_RESOURCEID
	   	  AND MV.VALUE2 =P_RATECATEGORY
	   	  AND MV.VALUE3 IS NULL
	   	  AND MV.VALUE4 IS NULL
	   	  AND MV.VALUE5 IS NULL
	   	  AND MV.VALUE6 IS NULL
	   	  AND MV.VALUE7 IS NULL
	   	  AND MV.VALUE8 IS NULL
	   	  AND MV.VALUE9 IS NULL
	   	  AND MV.VALUE10 IS NULL;
			BEGIN
				SELECT MATRIXKEY INTO V_MATRIXID
				FROM PPA_MATRIX 
				WHERE DESCRIPTION = P_MATRIXNAME;  
			EXCEPTION
				WHEN OTHERS THEN
				V_VALIDRESOURCE := 0;
				RAISE_APPLICATION_ERROR(-20000,'Matrix "' || P_MATRIXNAME || '" is not found.');
			END;
			
		IF V_MATRIXID IS NOT NULL THEN /* Checking for valid rate matrix*/
			FOR MATRIX_DEL IN (SELECT MV.MATRIXROWKEY, MV.FROMDATE, MV.TODATE,VALUE2 RATE_CATEGORY,VALUE1 RESOURCEID 
								FROM  PPA_MATRIXVALUES MV
								WHERE MV.MATRIXKEY =V_MATRIXID
								AND MV.VALUE1 = P_RESOURCEID
								--AND MV.VALUE2 =P_RATECATEGORY
							  )
				LOOP
					IF V_RESOURCECOUNT !=0 THEN	/* Checking for alteast one instance is there in rate matrix */	  
					   IF MATRIX_DEL.FROMDATE>P_TODATE AND TRUNC(P_TODATE)>=TRUNC(SYSDATE) THEN /* If Start Date of rate matrix is greater than the end date from file then delete the instance*/
							DELETE FROM PPA_MATRIXVALUES WHERE MATRIXROWKEY=MATRIX_DEL.MATRIXROWKEY;
					   END IF;
					END IF;
				END LOOP;
		ELSE /* If rate matrix is not valid then update error in staging table for that instance*/
			UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Invalid Matrix' WHERE STAGING_ID = P_STAGING_ID;
		END IF;
		COMMIT;
	END; /* Deleting the Rate matrix from CA PPM where Start Date greater than termination date End*/
	
	
	
	
	BEGIN 
		SELECT COUNT(*) INTO V_RESOURCECOUNT
			FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
			WHERE M.DESCRIPTION =P_MATRIXNAME
				AND MV.MATRIXKEY = M.MATRIXKEY
				AND MV.VALUE1 = P_RESOURCEID
				AND MV.VALUE2 =P_RATECATEGORY
				AND MV.VALUE3 IS NULL
				AND MV.VALUE4 IS NULL
				AND MV.VALUE5 IS NULL
				AND MV.VALUE6 IS NULL
				AND MV.VALUE7 IS NULL
				AND MV.VALUE8 IS NULL
				AND MV.VALUE9 IS NULL
				AND MV.VALUE10 IS NULL;
				
				
				
		IF V_RESOURCECOUNT=0 THEN /* Creating new Rate Matrix entry if the said resource is not there in CA PPM*/
			PAC_MNT_MATRIX_ROW_INSERT_SP(V_MATRIXID,P_STARTDATE, P_TODATE,P_RATE,P_RATE,P_RATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin',1); 
		ELSE 
		
		
		
		
		
			FOR V_CURR_CURRENTROW IN CURRCURRENTROW
			LOOP 
				V_CURRENTROWID  	 := V_CURR_CURRENTROW.MATRIXROWKEY	;
				V_CURRENTSTARTDATE 	 := V_CURR_CURRENTROW.FROMDATE		;
				V_CURRENTENDDATE   	 := V_CURR_CURRENTROW.TODATE		;
				V_CURRENTRATE      	 := V_CURR_CURRENTROW.RATE			;
				V_RATECATEGORY	 	 := V_CURR_CURRENTROW.RATE_CATEGORY ;
				V_RESOURCEID		 := V_CURR_CURRENTROW.RESOURCEID 	;
				
				
				
				BEGIN /*For existing and active resources, If start date and End date in the file is same as database and Rate is different. START */
					SELECT COUNT(*) INTO V_RESOURCECOUNT
					FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
					WHERE M.DESCRIPTION =P_MATRIXNAME
						AND MV.MATRIXKEY = M.MATRIXKEY
						AND MV.VALUE1 = P_RESOURCEID
						AND MV.VALUE2 =P_RATECATEGORY
						AND MV.VALUE3 IS NULL
						AND MV.VALUE4 IS NULL
						AND MV.VALUE5 IS NULL
						AND MV.VALUE6 IS NULL
						AND MV.VALUE7 IS NULL
						AND MV.VALUE8 IS NULL
						AND MV.VALUE9 IS NULL
						AND MV.VALUE10 IS NULL;
					BEGIN
						SELECT MATRIXKEY INTO V_MATRIXID
						FROM PPA_MATRIX 
						WHERE DESCRIPTION = P_MATRIXNAME;  
					EXCEPTION
						WHEN OTHERS THEN
						V_VALIDRESOURCE := 0;
						RAISE_APPLICATION_ERROR(-20000,'Matrix "' || P_MATRIXNAME || '" is not found.');
					END;
					IF V_MATRIXID IS NOT NULL THEN /* Checking for valid rate matrix*/ 
							BEGIN 
								IF V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY 
									AND V_CURRENTSTARTDATE=P_STARTDATE AND V_CURRENTENDDATE=P_TODATE AND V_CURRENTRATE!=P_RATE AND V_MATRIXID IS NOT NULL THEN 
									/* Update the record for same resource,rate category,startdate,finshdate and different rate*/
									PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,P_STARTDATE, P_TODATE,P_RATE,P_RATE,P_RATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin');
								END IF;
							END;
					ELSE /* If rate matrix is not valid then update error in staging table for that instance*/
						UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Invalid Matrix' WHERE STAGING_ID = P_STAGING_ID;
					END IF;
				END; /*For existing and active resources, If start date and End date in the file is same as database and Rate is different. END */ 

				
				BEGIN /* For existing and active resources, If Rate is different. START*/
					SELECT COUNT(*) INTO V_RESOURCECOUNT
					FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
					WHERE M.DESCRIPTION =P_MATRIXNAME
						AND MV.MATRIXKEY = M.MATRIXKEY
						AND MV.VALUE1 = P_RESOURCEID
						AND MV.VALUE2 =P_RATECATEGORY
						AND MV.VALUE3 IS NULL
						AND MV.VALUE4 IS NULL
						AND MV.VALUE5 IS NULL
						AND MV.VALUE6 IS NULL
						AND MV.VALUE7 IS NULL
						AND MV.VALUE8 IS NULL
						AND MV.VALUE9 IS NULL
						AND MV.VALUE10 IS NULL;
					BEGIN
						SELECT MATRIXKEY INTO V_MATRIXID
						FROM PPA_MATRIX 
						WHERE DESCRIPTION = P_MATRIXNAME;  
					EXCEPTION
						WHEN OTHERS THEN
						V_VALIDRESOURCE := 0;
						RAISE_APPLICATION_ERROR(-20000,'Matrix "' || P_MATRIXNAME || '" is not found.');
					END;
					IF V_MATRIXID IS NOT NULL THEN /* Checking for valid rate matrix*/
								IF V_RESOURCECOUNT !=0 AND V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY 
									AND V_CURRENTRATE!=P_RATE AND V_MATRIXID IS NOT NULL THEN
									IF P_TODATE < V_CURRENTSTARTDATE THEN /* Finish date is less than the Rate Matrix Start Date,Proceed and update error log*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file:Given End Date is less than the Rate Matrix Start Date' WHERE STAGING_ID = P_STAGING_ID;
									END IF;
									IF (P_STARTDATE BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE) AND P_STARTDATE!=V_CURRENTSTARTDATE THEN /* Start date is in between Rate Matrix Start and finish */
										PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,V_CURRENTSTARTDATE,P_STARTDATE-1,V_CURRENTRATE,V_CURRENTRATE,V_CURRENTRATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin'); 
										PAC_MNT_MATRIX_ROW_INSERT_SP(V_MATRIXID,P_STARTDATE, P_TODATE,P_RATE,P_RATE,P_RATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin',1); 
									ELSIF (P_STARTDATE = V_CURRENTSTARTDATE) AND 
										(P_TODATE  > V_CURRENTENDDATE)THEN /* IF Rate is different and Start Date is equal to Rate matrix start date and End Date is greater than Rate Matrix End Date then Error out the record*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: A Row with this information already exists' WHERE STAGING_ID = P_STAGING_ID;
									ELSIF (P_STARTDATE = V_CURRENTSTARTDATE) AND 
										(P_TODATE  < V_CURRENTENDDATE)THEN /* IF Rate is different and Start Date is equal to Rate matrix start date and End Date is less than Rate Matrix End Date then Error out the record*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: A Row with this information already exists' WHERE STAGING_ID = P_STAGING_ID;
									/*ELSIF (P_STARTDATE>V_CURRENTSTARTDATE AND P_STARTDATE<V_CURRENTENDDATE) AND
										(P_TODATE>V_CURRENTSTARTDATE AND P_TODATE<=V_CURRENTENDDATE)THEN 
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: A Row with this information already exists' WHERE STAGING_ID = P_STAGING_ID;
									*/
									ELSIF (P_STARTDATE NOT BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE) AND 
										(P_TODATE  BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE)THEN /* If Start Date is prior to Rate Matrix Start Date and End Date is in between Rate Matrix Start and End then Update the Error*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: FILE START DATE Cannot be greater than Rate Matrix START DATE' WHERE STAGING_ID = P_STAGING_ID;
									ELSIF (P_STARTDATE < V_CURRENTSTARTDATE) AND 
										(P_TODATE  BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE)THEN /* If Start Date is prior to Rate Matrix Start Date and End Date is in between Rate Matrix Start and End then Update the Error*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: FILE START DATE Cannot be greater than Rate Matrix START DATE' WHERE STAGING_ID = P_STAGING_ID;
									ELSIF  (P_STARTDATE NOT BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE) AND 
										(P_TODATE  NOT BETWEEN V_CURRENTSTARTDATE  AND V_CURRENTENDDATE) AND TRUNC(P_TODATE) >=TRUNC(SYSDATE) THEN /* IF Start Date and Finish Date are not between the Rate Matrix Start and Finish*/
										V_ROWEXISTS:=0;
										SELECT COUNT(*) INTO V_ROWEXISTS 
										FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
										WHERE M.DESCRIPTION    =P_MATRIXNAME
											AND MV.MATRIXKEY = M.MATRIXKEY
											AND MV.VALUE1    = P_RESOURCEID
											AND MV.VALUE2    =P_RATECATEGORY
											AND MV.FROMDATE  =P_STARTDATE
											AND MV.TODATE    =P_TODATE;
											IF V_ROWEXISTS=0 THEN 
												PAC_MNT_MATRIX_ROW_INSERT_SP(V_MATRIXID,P_STARTDATE, P_TODATE,P_RATE,P_RATE,P_RATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin',1); 				   
											END IF;
									COMMIT;
									END IF;
								END IF;
					ELSE
						UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Invalid Matrix' WHERE STAGING_ID = P_STAGING_ID;
					END IF;
				END;	/* For existing and active resources, if rate is different END*/
				
				
				
				
				BEGIN /* For existing and active resources, if new rate is same as old START*/
					SELECT COUNT(*) INTO V_RESOURCECOUNT
					FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
					WHERE M.DESCRIPTION =P_MATRIXNAME
						AND MV.MATRIXKEY = M.MATRIXKEY
						AND MV.VALUE1 = P_RESOURCEID
						AND MV.VALUE2 =P_RATECATEGORY
						AND MV.VALUE3 IS NULL
						AND MV.VALUE4 IS NULL
						AND MV.VALUE5 IS NULL
						AND MV.VALUE6 IS NULL
						AND MV.VALUE7 IS NULL
						AND MV.VALUE8 IS NULL
						AND MV.VALUE9 IS NULL
						AND MV.VALUE10 IS NULL;
					BEGIN
						SELECT MATRIXKEY INTO V_MATRIXID
						FROM PPA_MATRIX 
						WHERE DESCRIPTION = P_MATRIXNAME;  
					EXCEPTION
						WHEN OTHERS THEN
						V_VALIDRESOURCE := 0;
						RAISE_APPLICATION_ERROR(-20000,'Matrix "' || P_MATRIXNAME || '" is not found.');
					END;
					
					
					IF V_MATRIXID IS NOT NULL THEN /* Checking for valid rate matrix*/
					
						IF V_RESOURCECOUNT !=0 THEN	/* Checking for alteast one instance is there in rate matrix */	  
							IF V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								AND trunc(V_CURRENTSTARTDATE)=trunc(P_STARTDATE) AND V_CURRENTENDDATE!=P_TODATE 
								
								
								THEN /* Update the Rate Matrix End Date for the same resource,startdate,rate */
								
								IF P_TODATE < V_CURRENTSTARTDATE THEN /* Finish date is less than the Rate Matrix Start Date,Proceed and update error log*/
									UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: END DATE is less than the Rate Matrix START DATE' WHERE STAGING_ID = P_STAGING_ID;
									
									
									
								ELSIF 
								
								P_TODATE < V_CURRENTENDDATE THEN /* If Finish Date is less than Rate Matrix Finish Date*/
								
								
									IF TRUNC(P_TODATE)>TRUNC(SYSDATE) THEN /* If Finish Date is greater than system date*/
									
									
									
									
										PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,V_CURRENTSTARTDATE,P_TODATE,V_CURRENTRATE,V_CURRENTRATE,V_CURRENTRATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin'); 
								
								
								ELSIF TRUNC(P_TODATE)<=TRUNC(SYSDATE) THEN /* If Finish Date is less than system date*/

										PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,V_CURRENTSTARTDATE,P_TODATE ,V_CURRENTRATE,V_CURRENTRATE,V_CURRENTRATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin'); 
									END IF;
									
									
								ELSIF P_TODATE>V_CURRENTENDDATE THEN /* If Finish Date is greater than Rate Matrix Finish Date*/
									IF TRUNC(P_TODATE)>TRUNC(SYSDATE) THEN /* If Finish Date is greater than system date*/
										PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,V_CURRENTSTARTDATE,P_TODATE,V_CURRENTRATE,V_CURRENTRATE,V_CURRENTRATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin'); 
									ELSIF TRUNC(P_TODATE)<=TRUNC(SYSDATE) THEN /* If Finish Date is less than system date*/
										UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Given finish date is prior to current finish date' WHERE STAGING_ID = P_STAGING_ID;
									END IF;	
									
									
									
								END IF;
							ELSIF V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								  AND TRUNC(P_STARTDATE)>TRUNC(SYSDATE) AND P_STARTDATE BETWEEN V_CURRENTSTARTDATE AND V_CURRENTENDDATE THEN /* If the Start Date,Finish Date and Rate Matrix Start Date,Finish Date are greater than system date*/
									PAC_MNT_MATRIX_ROW_UPDATE_SP(V_CURRENTROWID,V_CURRENTSTARTDATE,P_TODATE,V_CURRENTRATE,V_CURRENTRATE,V_CURRENTRATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin'); 
							ELSIF V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								  AND P_STARTDATE>V_CURRENTSTARTDATE AND P_TODATE > V_CURRENTENDDATE AND P_STARTDATE>V_CURRENTENDDATE  THEN /* If the Start Date,Finish Date are not overlapping with any Rate Matrix Records Create that record*/
									V_ROWEXISTS:=0;
										SELECT COUNT(*) INTO V_ROWEXISTS 
										FROM PPA_MATRIX M, PPA_MATRIXVALUES MV
										WHERE M.DESCRIPTION    =P_MATRIXNAME
											AND MV.MATRIXKEY = M.MATRIXKEY
											AND MV.VALUE1    = P_RESOURCEID
											AND MV.VALUE2    =P_RATECATEGORY
											AND MV.TODATE    =P_TODATE
											AND MV.NUMVAL1   =P_RATE
											AND MV.NUMVAL2   =P_RATE
											AND MV.NUMVAL3   =P_RATE;
										IF V_ROWEXISTS=0 THEN 
											PAC_MNT_MATRIX_ROW_INSERT_SP(V_MATRIXID,P_STARTDATE, P_TODATE,P_RATE,P_RATE,P_RATE,null,P_RESOURCEID,P_RATECATEGORY,null,null,null,null,null,null,null,null,'USD','en','admin',1); 				   
										END IF;
							ELSIF (V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								  AND (trunc(P_STARTDATE) < trunc(V_CURRENTSTARTDATE))) THEN /* If Start Date is less than Rate Matrix Start Date*/
									UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Start Date cannot be prior to current Rate Matrixs Start Date Value:'|| V_CURRENTSTARTDATE || P_STARTDATE WHERE STAGING_ID = P_STAGING_ID;
							ELSIF (V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								  AND (TRUNC(P_STARTDATE) < TRUNC(SYSDATE)) AND (TRUNC(V_CURRENTSTARTDATE) < TRUNC(SYSDATE)) AND P_STARTDATE!=V_CURRENTSTARTDATE) THEN /* If Start Date and Rate Matrix start date are not same and both are less than system date*/
									UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Given start date is overlapping with existing rate matrix start date' WHERE STAGING_ID = P_STAGING_ID;
							/*ELSIF V_RESOURCEID=P_RESOURCEID AND V_RATECATEGORY=P_RATECATEGORY AND V_CURRENTRATE=P_RATE AND V_MATRIXID IS NOT NULL 
								  AND TRUNC(P_STARTDATE)<TRUNC(SYSDATE) AND TRUNC(V_CURRENTSTARTDATE)<TRUNC(SYSDATE) AND TRUNC(V_CURRENTENDDATE)> TRUNC(SYSDATE) THEN
									UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: START DATE is prior to current date' WHERE STAGING_ID = P_STAGING_ID;	
							*/
							END IF;
						END IF;
					ELSE /* If rate matrix is not valid then update error in staging table for that instance*/
						UPDATE Z_STG_RATE_MATRIX_FILE_DATA SET ERROR = 'Error file: Invalid Matrix' WHERE STAGING_ID = P_STAGING_ID;
					END IF;
				END;
			END LOOP;
		END IF;
	END; 
COMMIT;
END GD_PPM_RATE_MATRIX_SP;
