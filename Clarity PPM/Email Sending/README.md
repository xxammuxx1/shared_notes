## Running the jar
1. Download the EmailSender-x.y.z.jar jar
2. Run below command:
java -jar EmailSender-0.1.0.jar --senderEmail your@test.org --receiverEmail amit.chaurasia@test.org --ccEmail amit.chaurasia@test.org --smtpHost your.host.org --smtpPort 25 --smtpUser yourUser --subject "Email from Amit" --body "C:\\Users\\XYZ\\Documents\\Amit\\email-template.html" --attachmentPath "C:\\Users\\XYZ\\Documents\\RPM\\StoredProcedures\\NIKU.TR_ODF_PROJECT - PROD.sql,C:\\Users\\XYZ\\Documents\\RPM\\StoredProcedures\\NIKU.TR_ODF_PROJECT - DEV.sql"

## Source code for jar is available at: https://gitlab.com/hiamitchaurasia/email-sending
